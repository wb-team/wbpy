# test cases for the wanna-peruse log manager
## vim:set ai et sw=4:

# © 2009 Philipp Kern <pkern@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
"""
Tests for the logs module.

@contact: Debian Wanna-Build Admins <wb-team@buildd.debian.org>
@copyright: 2009,2018 Philipp Kern <pkern@debian.org>
@license: GNU General Public License version 2 or later
"""

from __future__ import with_statement

from contextlib import contextmanager
from string import Template
from io import StringIO
import bz2
import email
import os
import re
import shutil
import tempfile
import unittest

from debwb.logs import BuildLogFactory, BuildLog, MailedBuildLog, \
                       InvalidBuildLog, InvalidConfigurationOption

### MOCK OBJECTS ###############################################################


class MockLogger(object):
    def warn(self, message, *args, **kwawgs):
        message % args

    def critical(self, message, *args, **kwargs):
        message % args

    def info(self, message, *args, **kwargs):
        message % args


class MockQuery(object):
    def values(self, **kwargs):
        pass


class MockTable(object):
    def insert(self):
        return MockQuery()


class MockConnection(object):
    def execute(self, sql):
        pass


class MockDBFactory(object):
    def __call__(self, logger, config):
        return self

    def resolve_distribution_alias(self, suite):
        return suite

    def insert_pkg_history(self, arch, **kwargs):
        pass

    @property
    def conn(self):
        return MockConnection()


@contextmanager
def noop_wrapper(fn, mode=''):
    with open(fn, mode) as fp:
        yield fp


@contextmanager
def bz2_wrapper(fn, mode=''):
    with bz2.BZ2File(fn, mode) as fp:
        yield fp


class MockFactory(object):
    def __init__(self, compressor=noop_wrapper):
        self.logger = MockLogger()
        self.dbdir = tempfile.mkdtemp()
        self.dbmode_dir = 0o775
        self.dbmode_file = 0o775 & 0o666
        self.compress = (compressor, '')
        self.valid_suites = ["unstable", "experimental", "sid"]
        self.valid_arches = ["i386", "amd64", "s390"]
        self.logurl = "http://buildd/$package/$version/$arch/$timestamp"
        self.dbfactory = MockDBFactory()

    def cleanup(self):
        shutil.rmtree(self.dbdir)


class MockConfig(object):
    def __init__(self, compress_type="bzip2"):
        self._dbdir = tempfile.mkdtemp()
        self._config = {
            'buildlogs': {
                'compress': compress_type,
                'dbdir': self._dbdir,
                'dbmode': '0o775',
                'logurl': 'http://buildd/$package/$version/$arch/$timestamp'
            },
            'global': {
                'valid_suites': 'unstable experimental',
                'valid_arches': 'i386 amd64 s390',
            },
            'wanna-build': {
                'database': 'service=wanna-build-privileged',
            },
        }

    def get(self, section, key):
        return self._config[section][key]

    def has_option(self, section, key):
        if section in self._config and key in self._config[section]:
            return True
        else:
            return False

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.cleanup()
        # Do not ignore exceptions.
        return False

    def cleanup(self):
        shutil.rmtree(self._dbdir)


class MockOptions(object):
    pass


### TEST CASES #################################################################


class FactoryTestCase(unittest.TestCase):
    def runTest(self):
        logger = MockLogger()
        options = MockOptions()
        dbfactory = MockDBFactory()

        # Test the construction.
        with MockConfig(compress_type="gzip") as config:
            BuildLogFactory(logger, config, options, dbfactory)
        with MockConfig(compress_type="foo") as config:
            self.assertRaises(
                InvalidConfigurationOption,
                lambda: BuildLogFactory(logger, config, options, dbfactory))
        # Now continue with one and do some further checks.
        with MockConfig(compress_type="bzip2") as config:
            factory = BuildLogFactory(logger, config, options, dbfactory)
            # Check one correct...
            log = StringIO(SucceededLogTestCase.SucceededBuildLog)
            factory.create_from_mail(log)
            # ...and one junkish.
            log = StringIO(InvalidLogs.AttemptedBuildNoArch)
            factory.create_from_mail(log)


class BuildLogTestCase(unittest.TestCase):
    def setUp(self):
        self.factory = MockFactory()

    def tearDown(self):
        self.factory.cleanup()


class EmptyBuildLogTestCase(BuildLogTestCase):
    def runTest(self):
        buildlog = BuildLog(self.factory)
        self.assertEqual(buildlog.state, None)
        self.assertEqual(buildlog.package, None)
        self.assertEqual(buildlog.version, None)
        self.assertEqual(buildlog.suite, None)
        self.assertEqual(buildlog.arch, None)
        del buildlog.state
        del buildlog.package
        del buildlog.version
        del buildlog.suite
        del buildlog.arch


class SucceededLogTestCase(BuildLogTestCase):
    SucceededBuildLog = """\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for successful build of gobby_0.4.9-2+b2 on i386 (debian/unstable)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere

Automatic build of gobby_0.4.9-2+b2 on ninsei by sbuild/i386 99.999
Build started at 20090122-1148
******************************************************************************
"""

    SucceededBuildLogBase64 = """\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for successful build of gobby_0.4.9-2+b2 on i386 (debian/unstable)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere
Content-Transfer-Encoding: base64

QXV0b21hdGljIGJ1aWxkIG9mIGdvYmJ5XzAuNC45LTIrYjIgb24gbmluc2VpIGJ5IHNidWlsZC9p
Mzg2IDk5Ljk5OQpCdWlsZCBzdGFydGVkIGF0IDIwMDkwMTIyLTExNDg=
"""

    SucceededBuildLogQP = """\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for successful build of gobby_0.4.9-2+b2 on i386 (debian/unstable)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere
Content-Transfer-Encoding: quoted-printable

sbuild (Debian sbuild) 0.58.2 (18 Mar 2009) on buildd.net.philkern.de

=E2=95=94=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=97
=E2=95=91 gobby 0.4.9-2 (amd64)                                      06 Apr=
 2009 17:55 =E2=95=91
=E2=95=9A=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=
=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=
=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=90=
=E2=95=90=E2=95=90=E2=95=90=E2=95=90=E2=95=9D
"""

    def _check_succeeded_build_log(self, buildlog):
        # Check if various properties were parsed correctly.
        self.assertEqual(buildlog.state, 'successful')
        self.assertEqual(buildlog.package, 'gobby')
        self.assertEqual(buildlog.version, '0.4.9-2+b2')
        self.assertEqual(buildlog.arch, 'i386')
        self.assertEqual(buildlog.suite, 'unstable')
        self.assertEqual(buildlog.builder, 'something.local')
        self.assertEqual(buildlog.timestamp, 1232625407)
        # Commit the log to the database.  This should not raise any exception.
        filename = buildlog.store()
        self.assertEqual(buildlog.summary()[0],
                         'successful i386 build of gobby 0.4.9-2+b2')
        return filename

    def _check_build_log_contents(self, f1, f2):
        parser = email.parser.FeedParser()
        while True:
            line = f1.readline()
            if line.strip() == '':
                break
            parser.feed(line)
        hdr1 = parser.close()

        parser = email.parser.FeedParser()
        while True:
            line = f2.readline()
            if line.strip() == '':
                break
            parser.feed(line)
        hdr2 = parser.close()

        for hdr in hdr1.keys():
            if hdr == 'Received':
                continue
            assert hdr1.get(hdr) == hdr2.get(hdr)

        # Header equivalence checked, now on to the content.
        while True:
            line1 = f1.readline()
            line2 = f2.readline()
            assert line1 == line2
            if line1 == '' or line2 == '':
                break

    def testUnencodedLog(self):
        buildlog = MailedBuildLog(self.factory, StringIO(
            self.SucceededBuildLog))
        filename = self._check_succeeded_build_log(buildlog)
        with open(filename, 'r') as fp:
            self._check_build_log_contents(
                fp, StringIO(self.SucceededBuildLog))

    def testBase64EncodedLog(self):
        buildlog = MailedBuildLog(self.factory,
                                  StringIO(self.SucceededBuildLogBase64))
        filename = self._check_succeeded_build_log(buildlog)
        buf = StringIO()
        for line in self.SucceededBuildLogBase64.split("\n"):
            buf.write(line)
            buf.write("\n")
            if line.strip() == '':
                break
        buf.write(
            "Automatic build of gobby_0.4.9-2+b2 on ninsei by sbuild/i386 99.999\n"
        )
        buf.write("Build started at 20090122-1148")
        buf.seek(0)
        with open(filename, 'r') as fp:
            self._check_build_log_contents(fp, buf)

    def testQPEncodedLog(self):
        buildlog = MailedBuildLog(self.factory,
                                  StringIO(self.SucceededBuildLogQP))
        self._check_succeeded_build_log(buildlog)

    MIMEBuildLog = """\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Subject: Log for successful build of libinfinity_0.4.2-2 on amd64 (dist=sid)
MIME-Version: 1.0
Content-Transfer-Encoding: binary
Content-Type: multipart/mixed; boundary="_----------=_130098907776160"
X-Mailer: MIME::Lite 3.027 (F2.77; T1.30; A2.06; B3.08; Q3.08)
Date: Thu, 24 Mar 2011 18:51:17 +0100
From: pseudo buildd <buildd@invalid>
To: buildd@invalid

This is a multi-part message in MIME format.

--_----------=_130098907776160
Content-Disposition: inline; filename="libinfinity_0.4.2-2-amd64-20110324-1846.gz"
Content-Transfer-Encoding: base64
Content-Type: application/x-gzip; name="libinfinity_0.4.2-2-amd64-20110324-1846.gz"

H4sICCqJi00AA2ZvbwArLilN4gIASRApeQUAAAA=

--_----------=_130098907776160
Content-Disposition: inline; filename="libinfinity_0.4.2-2_amd64.changes"
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; name="libinfinity_0.4.2-2_amd64.changes"

--_----------=_130098907776160
Content-Disposition: inline; filename="libinfinity_0.4.2-2_amd64.changes"
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; name="libinfinity_0.4.2-2_amd64.changes"

Format: 1.8
Date: Wed, 09 Feb 2011 10:41:42 +0100
Source: libinfinity
Binary: libinfinity-0.4-dev libinfinity-0.4-doc libinfinity-0.4-dbg libinfinity-0.4-0 libinfgtk-0.4-0 infinoted-0.4 infinoted
Architecture: amd64
Version: 0.4.2-2
Distribution: sid-rebuild
Urgency: low
Maintainer: pseudo buildd <buildd@invalid>
Changed-By: Philipp Kern <pkern@debian.org>
Description:
 infinoted  - dedicated server for infinote-based collaborative editing (curren
 infinoted-0.4 - dedicated server for infinote-based collaborative editing
 libinfgtk-0.4-0 - infinote-based collaborative editing (Gtk widgets)
 libinfinity-0.4-0 - infinote-based collaborative editing
 libinfinity-0.4-dbg - infinote-based collaborative editing - debugging symbols
 libinfinity-0.4-dev - infinote-based collaborative editing - development files
 libinfinity-0.4-doc - infinote-based collaborative editing - documentation
Closes: 572690 603899
Changes:
 libinfinity (0.4.2-2) unstable; urgency=low
 . 
   * Stop installing the .la file.  (Closes: #603899)
   * Add a manpage for infinoted, taken from upstream git.  (Closes: #572690)
Checksums-Sha1:
 dcf3d6c2f1c216537df6c45d7191c656b8e3aa9d 546656 libinfinity-0.4-dev_0.4.2-2_amd64.deb
 e7cd59e285cd63678e6f2ccc84bb38c2f2f8651f 918920 libinfinity-0.4-dbg_0.4.2-2_amd64.deb
 8d9ec4c4c870f8a2942e8061d36ea6d1a1ca6981 307020 libinfinity-0.4-0_0.4.2-2_amd64.deb
 4584b17e9e6a24bfdbb80c74d64534afe6a416c5 129288 libinfgtk-0.4-0_0.4.2-2_amd64.deb
 28898ef526d5a250644c0383308d331ba8fef2e8 85100 infinoted-0.4_0.4.2-2_amd64.deb
Checksums-Sha256:
 c8a5c88e5d302880d84b9d1920883877f8b72a9f7bb39014cda77c0e6db3159f 546656 libinfinity-0.4-dev_0.4.2-2_amd64.deb
 4d93f4537b47a5fd213755fcdf1afdbf954b7a97151b9c467cae20e9c8da652c 918920 libinfinity-0.4-dbg_0.4.2-2_amd64.deb
 5a85fc8e2439707a1de7e6250362eda0f92ce3837670567f106ad657b81b9b5f 307020 libinfinity-0.4-0_0.4.2-2_amd64.deb
 4451465e931158abcd1051beca0efec367f865f54d05c135d3de67a4a3c1fe44 129288 libinfgtk-0.4-0_0.4.2-2_amd64.deb
 6cff37b5561a727792c5f03f65731fa5a4bdbc7c1418d96ad19bf55f7455aa10 85100 infinoted-0.4_0.4.2-2_amd64.deb
Files:
 a15ce20c2f7c2b308dcc7111007be1fb 546656 libdevel optional libinfinity-0.4-dev_0.4.2-2_amd64.deb
 038de34e9b543f4ba044e474817ed474 918920 debug extra libinfinity-0.4-dbg_0.4.2-2_amd64.deb
 a394a983536029130e7d3abff9e3fa50 307020 libs optional libinfinity-0.4-0_0.4.2-2_amd64.deb
 4159640ac4d4125607083b6f054b9185 129288 libs optional libinfgtk-0.4-0_0.4.2-2_amd64.deb
 ae797aca416ce20559a351e4e5cc6fa9 85100 net optional infinoted-0.4_0.4.2-2_amd64.deb

--_----------=_130098907776160--

"""

    def testMIMEBuildLog(self):
        buildlog = MailedBuildLog(self.factory, StringIO(self.MIMEBuildLog))
        self.assertEqual(buildlog.state, 'successful')
        self.assertEqual(buildlog.package, 'libinfinity')
        self.assertEqual(buildlog.version, '0.4.2-2')
        self.assertEqual(buildlog.arch, 'amd64')
        self.assertEqual(buildlog.suite, 'sid')
        self.assertEqual(buildlog.timestamp, 1300989077)
        filename = buildlog.store()
        self.assertEqual(buildlog.summary()[0],
                         'successful amd64 build of libinfinity 0.4.2-2')

        body = False
        with open(filename, 'r') as fp:
            for line in fp:
                if line.strip() == '':
                    body = True
                    continue
                if body:
                    assert line.strip() == 'stub'
                    break
        assert body is True

    ContentTransferEncodingBinary = """\
Return-path: <someone@biber.debian.org>
MIME-Version: 1.0
Content-Transfer-Encoding: binary
X-Mailer: MIME::Lite 3.027 (F2.77; B3.08; Q3.08)
Date: Wed, 20 Apr 2011 00:25:46 +0000
From: buildd on biber <someone@biber.debian.org>
To: logaddress
Subject: Log for successful build of genders_1.17-2 on i386 (dist=sid)
Key-Id: 44C3EAC7

sbuild (Debian sbuild) 0.61.0 (23 Feb 2011) on biber.debian.org

╔══════════════════════════════════════════════════════════════════════════════╗
║ genders 1.17-2 (i386)                                      20 Apr 2011 00:23 ║
╚══════════════════════════════════════════════════════════════════════════════╝

...
┌──────────────────────────────────────────────────────────────────────────────┐
│ Finished                                                                     │
└──────────────────────────────────────────────────────────────────────────────┘

Built successfully
────────────────────────────────────────────────────────────────────────────────

┌──────────────────────────────────────────────────────────────────────────────┐
│ Post Build                                                                   │
└──────────────────────────────────────────────────────────────────────────────┘

Purging /var/lib/schroot/mount/sid-i386-sbuild-00fbc416-7f6d-44e2-82c9-f804030b6a41/build/buildd-genders_1.17-2-i386-ggJwPx
Not cleaning session: cloned chroot in use

┌──────────────────────────────────────────────────────────────────────────────┐
│ Summary                                                                      │
└──────────────────────────────────────────────────────────────────────────────┘

Architecture: i386
Build-Space: 27784
Build-Time: 60
Distribution: sid
Install-Time: 40
Job: genders_1.17-2
Package: genders
Package-Time: 127
Source-Version: 1.17-2
Space: 27784
Status: successful
Version: 1.17-2
────────────────────────────────────────────────────────────────────────────────
Finished at 20110420-0025
Build needed 00:02:07, 27784k disc space
Signature with key '44C3EAC7' requested:
"""

    def testContentTransferEncodingBinary(self):
        buildlog = MailedBuildLog(self.factory,
                                  StringIO(self.ContentTransferEncodingBinary))
        self.assertEqual(buildlog.state, 'successful')
        self.assertEqual(buildlog.package, 'genders')
        self.assertEqual(buildlog.version, '1.17-2')
        self.assertEqual(buildlog.arch, 'i386')
        self.assertEqual(buildlog.suite, 'sid')
        self.assertEqual(buildlog.timestamp, 1303259146)
        filename = buildlog.store()
        self.assertEqual(buildlog.builder, 'biber.debian.org')
        self.assertEqual(buildlog.build_time, 2 * 60 + 7)
        self.assertEqual(buildlog.disk_space, 27784 * 1024)

    def testBZ2FileCompression(self):
        factory = MockFactory(compressor=bz2_wrapper)
        buildlog = MailedBuildLog(factory,
                                  StringIO(self.ContentTransferEncodingBinary))
        self.assertEqual(buildlog.state, 'successful')
        self.assertEqual(buildlog.package, 'genders')
        self.assertEqual(buildlog.version, '1.17-2')
        self.assertEqual(buildlog.arch, 'i386')
        self.assertEqual(buildlog.suite, 'sid')
        self.assertEqual(buildlog.timestamp, 1303259146)
        filename = buildlog.store()
        self.assertEqual(buildlog.builder, 'biber.debian.org')
        self.assertEqual(buildlog.build_time, 2 * 60 + 7)
        self.assertEqual(buildlog.disk_space, 27784 * 1024)


MockSucceededAlternativeSubjectBuildLog = StringIO("""\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for successful build of gobby_0.4.9-2 (dist=experimental)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere

Automatic build of gobby_0.4.9-2 on ninsei by sbuild/i386 99.999
Build started at 20090122-1148
******************************************************************************
""")


class SucceededAlternativeSubjectTestCase(BuildLogTestCase):
    def runTest(self):
        MailedBuildLog(self.factory, MockSucceededAlternativeSubjectBuildLog)


class AttemptedLogTestCase(BuildLogTestCase):
    MockAttemptedBuildLog = StringIO("""\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for attempted build of gobby_0.4.9-2 on i386 (debian/unstable)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere

Automatic build of gobby_0.4.9-2 on ninsei by sbuild/i386 99.999
Build started at 20090122-1148
******************************************************************************
""")

    def testMockAttempted(self):
        buildlog = MailedBuildLog(self.factory, self.MockAttemptedBuildLog)
        self.assertEqual(buildlog.state, 'failed')

    AttemptedMIMEBuildLogWithSummary = """\
Return-path: <buildd@random>
MIME-Version: 1.0
Content-Transfer-Encoding: binary
Content-Type: multipart/mixed; boundary="_----------=_131518399238140"
X-Mailer: MIME::Lite 3.027 (F2.77; B3.08; Q3.08)
Date: Mon, 5 Sep 2011 00:53:12 +0000
From: buildd on zandonai <buildd@zandonai>
To: somelogaddress
Subject: Log for attempted build of haskell-vector-algorithms_0.5.3-1 on s390 (dist=sid)
Key-Id: 2D941D26

This is a multi-part message in MIME format.

--_----------=_131518399238140
Content-Disposition: inline; filename="haskell-vector-algorithms_0.5.3-1-s390-20110905-0036.gz"
Content-Transfer-Encoding: base64
Content-Type: application/x-gzip; name="haskell-vector-algorithms_0.5.3-1-s390-20110905-0036.gz"

H4sIAAAAAAAA/+09a4/cxpGfM7+iY9zFu5bI4WPeiQLIu5Kjiy0LXjkJoDN4
HLJnhlkOSZCc3ZVgBIf7fB9ygIMLDrg/519yVdVNsvmanZlVIh8UYSXN9KOq
urqquqr6sdlyF4Q+O7vky8CNWEZfz5mhT0zdYGeWzZ7zJbMM0zxnccTeuZEf
R26g+9Rej9P1YPDjX3748S9//mh+/hsG/F9s42bXPAy1G+7lcaq54TpOg3yz
zYB3Y93WTHaW2XPjnLX/GGN2xRNiKjOMhT1hABCA/s+HHtnf8+d/B4NXrnft
rvmin5eD3/E0C+JoUTB1cBXvUo+zVvnT1NsEOfTfpQAQOT/4gucLk23yPFkM
h6s8AZlVxJbtoix3lyFnL6JveMjdjLM35mjCrj//jrpaRdcg8uJtEK2P6Wzf
j3e4dYOIifFk7M14NJlT9xfraB/mCiMhGu1tK1vq62TN3sxA0CR540PG1kcg
ApgcMD4vjvI0WCoQbL2CMD0AQhRH2irlvAIxH+tmCWJ2KJNRHJiUNgAyHU3G
JZD5ESNpwBmP9Flzyg6A8jp1oyx0c5DfF5HP74SgGocOptmdvbGM0Zg9hAyN
R4II8yC52D+x5kGKc9/cmvbhIrpves29CnLKDB8Ep3uOj1C7/lkmSJND1B5M
0sQYlX2me/uU02DXpmG2t1PFKXs+fiijUArvM36N5s957m24z6wpOCtffc6A
cdYkY2emYeO8DbPzwTfc9QEKSwSpLAyyPNN1HdyWH/7zxx/+/aP5+TMM+D8Y
cYxlYhVdBSHM3el/ACAA/eFDj+zv+fPXwWBwseHeNXv66vVgT1PZDEXPvXGD
kLwFyfcb4b0IMbyMb6Mwdv36pNyC/3Mfigf+DPp143OMArDG5wkH8xN5b1kO
xhrrik6gyDkHjVvF6ZYUEitffv36xcWzBfu016X7VGJDEEHG0Njl8BeUGJQ3
33D26aWbetBMMomRpYhDlr3Ncr5lbr4YSAvhY0PFPAyT67UmEQ/7fcqXHJDl
MVvzXBiO689ZvCrY76IjecMz/R7/cSg+Dht2+4CwwM+8czDNs5HFmq7me8SS
uylgsSx9eoBT+oDBBKsV4rHnNJqaQb4mg2xkGEZOpTkupR2MexJykCCIKLGZ
X1TEUfiWbWOfF4qGM3KPGgw+Umv+AqcrDIGZKWciki81NgArcgbKxdPIDVnK
szgEleqKRz9qa06mTrskrmULwUSNZxmP8sANH7OVe83TOM4rcw4Gj7lhCmbw
LQguTQCv8x0tYQPQoqPLmWnq4/NBgaG7iW5ONPO8jl2xymAdV2HgCbMtxYFM
epwFOdixGl0L9jcw+QbbJevU9bn/mBks4rehMgAsAlOb8m18I1R9ZLIozss+
CBzqEHrE1+7/D5LLyQC9S8E2RjDNuIQ0ZeDjNkr9q8cRluofRgmNEgRzGvmF
j8XnxM2yW5++bB6T9d/lQQi1PhWI1bwoghW6+AgO0mPGrVWWpPEaClZB5Mu6
dcoT+PddAP9u4iyP3C1/zCJvl2Y80xCr8i2IHrOEp6Esz1BrwniNxeCmgTYH
uSbhFt8fM/BIgOKsQLiNdxGU4jcNTNbuDkAES2+i+fyGfU+f8SPQ5Hns7NdP
2Ggx0ke6fQ4ljx41SrZgQ8X4qDtWguW0wbwSNzY8BHKpeAolnr8EAgr5hA6Z
lwZJnlEDQ58hio1H/2jAqRVRhl+kKBcoDH1y3ln3q19BnWm0KmvAkjTYko1W
4NnnfdUEctxVjUCPXp1KiepcdSa4LimS1tkIuKtbNrXbdDYY6aYG9aV4djaa
6WNc3/w+IOhpjgHQVB9Bq0qwezAaugXNCpHvXlIXtm4QThCX3kVXx8kYlLrS
025k/en3L15pmMrXjKlmWAi31KoeGkc6NHu0hJaodJ2NLH2OkFAd+zBjfaGo
PfNjTM4HqgZ3NhsLVIpu72tWan1PI9MCymAKwCT0jN6CyZyfD8hc9MwPCA4y
6VF2E9nWzEYhqpuVHp7NZn620kDtTbPqcUhbME09bLbGiL2wWj30wpDmMCrQ
GTJqPRiLNpXFu6dhaQ772kEEBs3APnazGvg4AUgWq0wlhtwZ+EHZKuA+dH30
6NSuaHF7xG5majNd6lcv+YWOsdJUNxCUZnvBtkGWgW0bfJsJb2/l7sK8TBDM
9Lk+GaBNr1q2bft9UMDs6/MBWNaqYWH+q5KWqb8f6lyz3k+3Tkpqi8T9UNE+
mpr5vnvXSXtPoUq1bOPUdq3WQES5RHeswveVqN3qi+1hhbTuvv/g5PWGA+fC
ML7FJvwuT90COqYFwbFe8kqRFgOGPhWma8hCMd/LwULncYyfN9pSkLGKKa+I
Oa8cQBb/kyUnHm5c34+9a4C2hmGtRMUm8+Iw3qVsk29D7Q7+Chz41SI44LWH
iEsTKzMyCahBfgEk8cUgVy6NvdjGT6tVULATPo6JrfDXgiWZWq63ScnubWIa
Ag58vLsbhf4yxG9bdx14Jn5KvJQT2CRIOEgTp9JdBLOQAvsIItBtgYMYaf4S
gCUx0oqyOLjarYGnOVijgr/IzVtYT9PAg8HcxqmP88luNzHYphvXo0liiggC
qNxdE6fRIJZ8BV4iK4Gx8KmSXfhM7O2QMKhqCCyBKNhQNNgmKynLGHPe3t5q
yzS+zUBLiC+wsizjOw1XaZAyL95uUfbqA3R3eUzyQXBg2Q/ZLeZCw7fRnQZf
qUESw9Sqk+O7ucuyjZtyX9sGW66hUAE0lApKPxX4M8BIH4iIujS/fPb7Y2SZ
FL8m0KVN6BVtZHtDuFUBrwyGKudt21KIPoA7Qfjboi+mskv4DzQ07HB71qNO
QmsEoCP1RtWa+oy2ZrPIkiwU4a1r8MBS0i/2+P78iz1v5F/UDL5pm7jzB0Ov
MvdPVzmISL4BpY1BWEhrH7OJYRUtfT/AMjeE6Di7ZhmMglcjyBDHSdn/GkNp
Lxk8SJgKGzdIKSN+YsK/nDICCt6VpY0wuz87PblfyCBBtHFpN/6UeiYRCx5o
BXl0AmQx7QTaWowxKnvkr7K1ZgLRI6sEPT4NdLEUdMOHiPGAcyo9CGr2RzLb
Ai8VwB5ydqUHqmJrCqkwtQnw2RxPDjjO0gNV6qbggo7etAUe+xvTsOYHHG/p
57BcXgku8HakjSFYxYmzq3MB/cdVeuCSjW6CHBvqWYOj1a1m6Ak2wAPlAC91
hOex7I5DKUfwgQyg5O4UwgOSMAA8m847zqkcvKlWrh9SFGzdciGQw4kzO86t
HKMapeWXVFszfYI6YU5sheRTtE46cJLJE8xgAFjF/phHq1p9wZGQ57qNLB7Z
CiuOVzfp5bZEwjKmdsfZlkPhNhd8zHJDCAsC/cgyjIkxNVGe39iGYj3NozWw
WmsJgYmmefIo2u5QB62KfutoHazcJwRMcTPo4KSyx9bRKkgumuDDSJ8DuOlY
Gbx1tNqp3p4UYpinN6YiD9bRKldzFgWxc4A51pWRn6Jv5OJJIaOcIp0YG1VA
T9G0woWswzVN5WTqKapWuJ6dy71RrZjW0dqmeJmdC/J4qpB+tM6h/05gpwB0
hIvm2Ib1+KsC4NHqpQYDdcC4HVsCto/WrjKyqEMdTfVpBfX4AyX16INgT6Ss
2dW02cefISmCe7kMmXNKSuPJFKPyVO2TD40owZRQuRkqnW0qbrB90iLXCpik
psj0FC78lV7bJ7mYrfCrjcNWhnGKPjZCuWL5QwfONizl0M5J+tiIClXo1lT6
RXKRwUxb6L5Vk1hYHKx3ReCUBREejEpk8h3ztBiPlRFbeepHxmJAg42nMO0x
HQrFYz9XPARyCEfKb4J4l0HI5/OMiqv8RC2A0gdnRaIMkw/k5Om6zsBazWx5
TA7DQz9Iaai4jezt0pTDKq3Ek/r54NsI4SOgWoB2tkrjLYIcqsWODNkcZBpy
+xybHDUCjNT0BlaK3uoYscgRsdzpyDCGa+CisK6GCkucmtFvIHyV8sRFR0yE
3kmIAXEZxwmLbgqLPmVnO0oQS9jUxLH+2XZVs9+AX9EngW+5TC8RgntIKOK9
++iQ7R5CTIHqmFlQY0YVei2WrOZDLXYoujxx9quoUsWqxJoVzqrQEbHniShF
yKmik0FohUoUOGVAerpoizi0IdwyOK2JtyhzysDyRJRoUlRsFLJWiPDrg3Go
IWtt1tRQVpk3pdipIpnTeYoxbYOjFObW+IkljhrznoivDHVVjFX8W+Esy5wy
HD59iGUU3BhnFR3XBlsWO0W8fDpqESU38MrQuYZUlDkykj4dYRVAN5AqkXUN
cVXuyGD7YYLcIcNt8X245DYibxVrMyivsDdqnFao7oCDcDwtVXyukqFE7RUF
VaGjhPGnoS2DdxWrcrapRFqWORThn4YN43oVEcX5FY4i1z7EckfE/icOqwr4
awNT8gDK0KpSBxMDJ0qTmg+oIVXzBApWpRi15rSBynxBQ08phVDTUCxxZNx/
umGQWYQ2NnGarIEQCx+OU6YY2t5oG6cs3OuTHmPyRf6hafBlVqJu7kXhfT7i
wWZw49VM4Mbr0hIodmRC4HQ8Rdaiga9MZihmtyp9D2gxgmzipKiyjhCLHoit
nutoOg1KFqTuOVQVzuQBElwkRGp4iySJglEWOWXK5FR8rURJDXPH6dSKhlal
Q/mVky1TK6nSVKX2gdSaUjWrnTJV8gClbmVh9hLVEMnO+vdFVpW46SBJPQjc
JKeqcygd817I6GGNmgDqI0QypYuSnOjYJY2cicyOdLYSOQ6R0ehqIBITNZvf
1UzkBM7qeeSehkW8fl/reixOUbdos0t8N+eaG9L1A5SUbMFEYmG4y9LhMogw
Ytdu0yDnmJoAlt0EPq9qRc0Z/XeOyS88P0M32A6HTtPTBZwqaPaaoJXBqSG/
CO9b4y/C9DIsFy2qU2pFhg2EBdru3BCEDc91tLleROBlKNxCJgLn/vp6sFsF
B11zLKJTNRhttVIiyjJ87BSXKgAsQj3R7GXMMm/Dt67MKq7iXeQvmB9j3yjO
N/B/E1gR1ckQrgufGoPJYKuPFXu50IpwWoFMq4sajSiRR6udEj9QsNBqIPx+
4eC3e6tOOvrj7RY1jxqY0MUn4QNL97OngTCne9qUbuYh5kW0vMdkkC8nHZrD
bQX26lLkYsnGe68gZMDwZklTwV/v0ohA37gpmm3yspAeeyjXAZ2CULw+C51c
lr3dLuMwwHOC0TUSofZs9Bn2gx+9F/B1RlZOao2hjUZiqepr0fQBJ52iUHls
pX/WbtPhW5H/1Ckubden9CLuby+GdG8H1Weg5fiehgXYsuVP/xZl6xTfobco
fwvKxsMF+xKvTNBBJ9vSxhr6K3diZ4te2ro7H7wGS+Rt6AyFHGXx0sKCLYuM
O5h+Ux/DMmGYpjEzxppZ3htzyksJ60ePtJE+ccQ1iAneQat9D4urGY68hoEl
We57jx7VGski7EutqyoYixbKO24UBoMwFy9iKWS7SU6uvTnWx8o9QAh0xupV
QEdcxqILgQ7du9o74PYWRHkxz6muszQvQzu0Qb58FyQWpbPGMBAl/1PdQ3To
UhfzksSpbpLAtxoL8caXo9zuYs3kDqtSZ2N9ZBZfNR7BQg59sdimVqs4A4uM
GWsNZpA1EmDyOqQmo3Htmr/FRdkBfgDXYfQz9cKkQ3fIWDPnU12jdOT9MZKa
SmLaMoR3x1AEnM5rYtU1zJ764lq4I66As/r2Q3V106kulKHYKhzvEGK1lkR6
JLYWEMZEm8rCcVGIjzyMW8IvvxeNZGEr1ct6Ni+Ykg1hHakK1swmsHUEVgPZ
pJso2etkfaN8S3ni0H051trlovusDl2W6zD6ImYub7uiFk5YO7xnXTkG1rFl
wfC6WQG7dsMM7WLGUxIOkC8NxLvp1bWz02g/XC80UYH1MQ4VC/I8NUlHYcom
omwZXgc+NpNq287pFV5W7fs7EBrdKDWZjBEYDdWikXVqFU7q35pm0AvjeK0l
SYjoTLw3SFSCm8PT1OqR9taeCJb4S7ANIPN0Dc8URahmeLOg0rO+dGIRcbZL
157pASGLKbBrJgo8ZKpiuNf+cmsDjhk+c2J0+gFVMqF72W/XN6P/jkVdqWjt
SynOq9M4ZFXtw3eVl/vi7UrwPU11wQrfbV0LzQxKaRXtKYYHy/CWY13gtglw
1CjmGm+pjATfRbW8XTp26BopJQvcrQZ+7g4XM5hKYLPVKCa5a1SlgDjY8kax
sa4VKOcqasmL4mQHFSbgeI9QQKFQtkvCeb3gNhzXCvASI0KaOBM6ao3WndNS
jsyA0c9o/aHiJA5F4UgKcxa60doiZR7JvtkebTjKmVgiUyg2kaoBVkSqeXvD
j0qzpWbQfpy1MNAsA1bShN2uaUpam8E9zot4AMDpuL3LQsBGVtlGtk3ptr5T
3BNlzQMDUHB7TVaVLAoJW0XRdheCLwJLuZbtkiRO89L0qDedpZwpl5pliXSX
uqhM3NzbEBm4PBIji/vM1WMHjaJCgqvSzh0/VkgOBnvbOJLyk3HfkRei4XOU
gfpxucgAS8FzyXZ+DHDAO0pQ//FKs5Z6jbWluOjcU6wVXqBambu4wOHlZpa/
w9wPekPmBr5Wt5Mrnt+9k0AKu/AOJt8EpVugRtnAR5IM+yN98IQiqAc8o/eP
V5gGMnstH2E79NG7wQBdwQUDj34ZYnYB346hV9z+bbgBd2NILrw/1MmBHObp
Dm97QusMX8T9dCESlTGEmwxckziV4K6CdeTiQ8JgiXzOXm929F4zM5k5W4yN
hWGIt5u/fX0hsz+XV0+RCPbiko2mI9syJhcS1oUbfZozjx4yywq4C5bslphJ
wT4YFFPCUdyXFwNYsFuX0jNAoxuE4robRIXB6m0FBh/k1vsfunPkI226n3l1
2BjULMTVYrHb0P9Uz75X4DRC0AV619rG6qFOh29rHayRvn73IEA4THE0RAKT
j8f5QeaJ+31Hv6QIUkkUkRDJhMIpzzF2gEHm49rFLp5/+fSLK0ZZ/qrVKsQ7
xWfIG3wj44ZHfpyeL5i2ZtrX1j5wr14dBXAfqD/84f3S9vz9gvvy8sEjlbai
uR/bfjGyv2/1OIJ4hpwpMsw0bclXccpF18P0qI4FQ1Nxr1U+bS5eNi9zAzJx
MUzRCWFeyN1okHPoo93Vqgb+xsFyesgOPlNLNnjDfo4tP5FNN+jiuulbPeP5
LvmEfce+/5511klU6ZZp6Qp1LKd/NEx+04fNbp31dL3Cf/VNUHyI5YeY6Z+J
g+efEeCVTD9hgJTl7jaR3xF0vUCkD0RZSZPAnUO4hDlJhCKhyhqyfprINWb1
qpLgr4DNuEQ0gAID5Vw6GbYFINmgPhVEWfdUbK/9IGVawj7RPxnQ9gDdqqeT
VENTNBILF+bT9O31wjTmC/b7p9+8fPHyiwVjl88+dy6+/urVN8+urpxnf7j4
8tvLZ3gm38XMaco9F3eKb9w0wCsBg2DFiAyYaS45HW4yprlqySb7Jb69Csug
B/PxCe6H0VyJZIZYnn7+yS9B94Kcmb+EdXOAj3uIRvhIfAm3gufHrMANeP6J
2kosKCaaeDBBlDMt7pYWidJAlAgy4oM3JiaJze/YBe2b4NrwFaZ81T9nFUWP
S/E6H3wZRMWpuQ7BxFxzj7TL+xCo0kT7jcX+dfCzn2kQsoOA3D3BaYQ6lIUg
pW+0L1JofHFJnjZKoEJ2Ft4JdCh1R5RLga6Bkk9JDGXdsMeIDAsQ+TYs+wv5
wt71fIPSmUBDnyGMgkcoOJpkAWUlBJtp8Fqc4AbAE+0S5PDF05fOy6+dL35z
8YIQMza4kLzCDj1EIqOfk50GaYO4Bxd78fgf+UnZk+duiM/K7aLMXfF62RJF
MZNlr9MdH1xW2xa0n/zrJzb7xS/Yr8bF1hylyUeAFgKyWuu3ZBKxyZMnGBt/
VvYoazQsh/C41rFM8wAuTEAhNhhY0bvKAskEkNpXsIQ6TmRH0yh6Sn4BTvkG
z4W7dEPMKRiYuWByp9AHApncrZMNiz3EBSt38WSN3JFhQlIXtCE5+DyIXLov
Uz3IhKtlsVkJioJz32owuFeye+WyIutVGtzgbtJyDw0Aid9xb3CJj46IPfmO
VkKu+5bTy9jb4cUO8WZLb3eU/D4QL/G2KL+T7rngp5uKr+h8V09SV/u8bord
PGsjjxHQtyRRv65WAa2PKlh85LBwJiivsx8LNC86bqqOxOB7Om6Ujhp4HEd2
xi44hnXKQZ7d1K+NotjlLWBaoD3WfpjFw0MAc+MmyduKTRtaIGrgM+Jr5XRN
pvcApw5Vb7EzXPTHDP/+/r/JLsQrMEhONWXw7Y/AdpW00N8PKaReYdlLfKEJ
KAuijTefVV/RHRQrTw1T6ka4hOzFJtrIDmjJkv3tqYlsnvdJODbMhXjvynF0
L5jC4e1Y4eiWVxp7PMvkSTXstW+hKLen97QRLsFcdQnQcui/oy7607KLfkHp
N/APsH4o6odV/VDU6xt6N1WQLHyx4f4O6F28sQ6l4hvXD+76iaDqI2gQ7YkE
+1ASrjiGFf00iPojiJAdiIrRoVR8Db7E1g37yZANjqCj6EGEjA8l5EWU8ZQW
il5SyiZHEFP1IXImh5LzVD5Bhl5SP0VqqyOIqnUjuqaH0vUb7ib99GDtEXRQ
c8I/O3yaIITdN0VQfdT0YHsiYX4oCV/xdM37SaDqI0gQ7YmED27EEucnYMYk
ER/ckEk6fgqmTJLy0zFmkqCfoDmTlH1AgyYp+LAmTRBxE6Q5HuTe8m0MTha/
27i0AUK7ErjtAAFRjOki2UK+6fvZZ5+xN43c23fsGW6QMLMzI4tVi44EGFu7
ECRTtcjj4DnCXcasv+FvNfpp/gg3Fg9tBRm+lOHmtIVkzI2xZhhje/D86Ysv
n12yNy32wrxz/7uPj2Gvdum6djw58zaYvh7SeYBhFvh0AlQTv7FWGxvWzPCs
kTY1ua2Nxt5cm638pcb5ajJx56uJz6dSc0RQot27oSTgz95t3n79Wwh6cpH0
pogKwxf87aNeGONvrhKUYSJ0l/GPdBP8arfdYij3j03w037+Ouj6HbbiF4Fc
4f7lgo1Gk8lMFr0O8NX/2dwaXNL5nuUuJ4kEvRg8x9d3r3KyzWIjQr62LXuN
p4N/iZd7fu1uoQGH/IZe2USCns8m8lf0au1f3auO4opWggUYwpxvE1iU2r/q
90PPyN/7Z/C8d3UQq0fEOb7lbBgLc7KwJo8FK2t73P8HCZBNB897AAA=

--_----------=_131518399238140
Content-Disposition: inline; filename="haskell-vector-algorithms_0.5.3-1-s390-20110905-0036.summary"
Content-Length: 271
Content-Transfer-Encoding: binary
Content-Type: text/plain; name="haskell-vector-algorithms_0.5.3-1-s390-20110905-0036.summary"

Architecture: s390
Build-Space: 44668
Build-Time: 892
Distribution: sid
Fail-Stage: build
Install-Time: 57
Job: haskell-vector-algorithms_0.5.3-1
Package: haskell-vector-algorithms
Package-Time: 986
Source-Version: 0.5.3-1
Space: 44668
Status: attempted
Version: 0.5.3-1

--_----------=_131518399238140--

"""

    def testMIMEBuildLogWithSummary(self):
        buildlog = MailedBuildLog(self.factory,
                                  StringIO(
                                      self.AttemptedMIMEBuildLogWithSummary))
        self.assertEqual(buildlog.state, 'failed')
        self.assertEqual(buildlog.package, 'haskell-vector-algorithms')
        self.assertEqual(buildlog.version, '0.5.3-1')
        self.assertEqual(buildlog.arch, 's390')
        self.assertEqual(buildlog.suite, 'sid')
        self.assertEqual(buildlog.timestamp, 1315183992)
        filename = buildlog.store()
        self.assertEqual(
            buildlog.summary()[0],
            'failed s390 build of haskell-vector-algorithms 0.5.3-1')
        self.assertEqual(buildlog.disk_space, 44668 * 1024)
        self.assertEqual(buildlog.build_time, 16 * 60 + 26)


class InvalidLogs(BuildLogTestCase):
    def testEmptyLog(self):
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(""))

    InvalidHeaderTemplate = Template("""\
Return-Path: <i.am.no.good@citizen.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: $subject
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: oh.noes@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere

Good job opportunities here!
""")
    ValidSubject = "Log for attempted build of gobby_0.4.9-2 on i386 (debian/experimental)"

    def testInvalidSubject(self):
        text = self.InvalidHeaderTemplate.substitute(subject="unhelpful junk")
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(text))

    def testValidSubject(self):
        text = self.InvalidHeaderTemplate.substitute(subject=self.ValidSubject)
        MailedBuildLog(self.factory, StringIO(text))

    def testNoSender(self):
        text = self.InvalidHeaderTemplate.substitute(subject=self.ValidSubject)
        # remove the From header entirely
        text = re.sub(r'From: .*\n', '', text)
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(text))

    InvalidStateSubject = "Log for foolish build of gobby_0.4.9-2 (dist=experimental)"

    def testInvalidState(self):
        text = self.InvalidHeaderTemplate.substitute(
            subject=self.InvalidStateSubject)
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(text))

    AttemptedBuildNoArch = """\
Return-Path: <buildd@something.local>
Received: from world by raff.debian.org with esmtp (Exim 4.63)       (envelope-from <buildd@cyberhqz.com>)   id 1LPyAy-0004pv-OF     for logs@buildd.somewhere; Thu, 22 Jan 2009 11:56:50 +0000
Subject: Log for attempted build of gobby_0.4.9-2 (dist=experimental)
Message-ID: <20090122115647.4A47DB8413701@buildd.something.local>
Date: Thu, 22 Jan 2009 03:56:47 -0800 (PST)
From: buildd@something.local (Debian/i386 Build Daemon)
To: undisclosed-recipients:;
Delivered-To: logs@buildd.somewhere

Automatic build of gobby_0.4.9-2 on ninsei by sbuild 99.999
Build started at 20090122-1148
******************************************************************************
"""

    def testAlternativeSubjectNoArch(self):
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(self.AttemptedBuildNoArch))

    InvalidArchSubject = "Log for successful build of gobby_0.4.9-2+b2 on ubuntu (debian/unstable)"

    def testInvalidArch(self):
        text = self.InvalidHeaderTemplate.substitute(
            subject=self.InvalidArchSubject)
        self.assertRaises(InvalidBuildLog, MailedBuildLog, self.factory,
                          StringIO(text))


#LogsTestSuite = unittest.TestSuite()

#LogsTestSuite.addTest(EmptyBuildLogTestCase())

#LogsTestSuite.addTest(InvalidLogs('testEmptyLog'))
#LogsTestSuite.addTest(InvalidLogs('testInvalidSubject'))
#LogsTestSuite.addTest(InvalidLogs('testNoSender'))
#LogsTestSuite.addTest(InvalidLogs('testInvalidState'))
#LogsTestSuite.addTest(InvalidLogs('testAlternativeSubjectNoArch'))

#LogsTestSuite.addTest(SucceededLogTestCase('testUnencodedLog'))
#LogsTestSuite.addTest(SucceededLogTestCase('testBase64EncodedLog'))
#LogsTestSuite.addTest(SucceededLogTestCase('testQPEncodedLog'))
#LogsTestSuite.addTest(SucceededAlternativeSubjectTestCase())
#LogsTestSuite.addTest(AttemptedLogTestCase())
