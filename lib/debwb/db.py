# interface with the PostgreSQL wanna-build database
## vim:set ai et sw=4 ts=4:

from sqlalchemy import create_engine, MetaData, Table, String, DateTime, \
                       Integer, Column, select, and_, func, desc, asc, types
import psycopg2
import time


class DebVersion(types.UserDefinedType):
    def get_col_spec(self):
        return 'DEBVERSION'

    def bind_processor(self, dialect):
        return None

    def result_processor(self, dialect, coltype):
        return None


try:
    from sqlalchemy.dialects.postgresql import base as postgres
except ImportError:
    from sqlalchemy.databases import postgres

postgres.ischema_names['debversion'] = DebVersion


class DBFactory(object):
    def __init__(self, logger, config):
        self.logger = logger
        self.config = config

        self.database = self.config.get("wanna-build", "database")
        self.engine = create_engine('postgresql:///', creator=self._connect)
        self.metadata = MetaData(self.engine)

        self.tables = dict()
        self.tables['pg_tables'] = Table(
            'pg_tables', self.metadata, autoload=True)
        self.tables['lastlog'] = Table('lastlog', self.metadata, autoload=True)
        self.tables['architectures'] = Table(
            'architectures', self.metadata, autoload=True)
        self.tables['distributions'] = Table(
            'distributions', self.metadata, autoload=True)
        self.tables['distribution_aliases'] = Table(
            'distribution_aliases', self.metadata, autoload=True)
        self.tables['distribution_architectures'] = Table(
            'distribution_architectures', self.metadata, autoload=True)
        self.tables['distribution_architectures_statistics'] = Table(
            'distribution_architectures_statistics',
            self.metadata,
            autoload=True)

        self._dist_archs = None

    def _connect(self):
        return psycopg2.connect(self.database)

    @property
    def conn(self):
        return self.engine.connect()

    def get_pkg_history(self, arch):
        return Table(
            'pkg_history',
            self.metadata,
            Column('package', String, primary_key=True),
            Column('distribution', String, primary_key=True),
            Column('version', String, primary_key=True),
            Column('timestamp', DateTime, primary_key=True),
            Column('result', String),
            Column('builder', String),
            Column('build_time', Integer),
            Column('disk_space', Integer),
            schema=arch)

    def insert_pkg_history(self, arch, **kwargs):
        tbl = self.get_pkg_history(arch)
        sql = tbl.insert().values(**kwargs)
        self.conn.execute(sql)

    def get_packages_tbl(self, arch):
        return Table(
            'packages',
            self.metadata,
            schema=('%s_public' % arch),
            autoload=True)

    def get_lastlog(self):
        qry = select([self.tables['lastlog']])
        return self.conn.execute(qry).fetchall()

    def get_pkg_history_archs(self):
        qry = select([self.tables['pg_tables'].c.schemaname],
                     self.tables['pg_tables'].c.tablename == 'pkg_history')
        return [row[0] for row in self.conn.execute(qry).fetchall()]

    @property
    def architectures(self):
        qry = select([self.tables['distribution_architectures'].c.architecture])\
                    .group_by(self.tables['distribution_architectures'].c.architecture)\
                    .order_by(asc(self.tables['distribution_architectures'].c.architecture))
        return sorted([row[0] for row in self.conn.execute(qry).fetchall()])

    @property
    def distributions(self):
        qry = select([self.tables['distributions'].c.distribution],
                     self.tables['distributions'].c.public == True)\
                     .order_by(desc(self.tables['distributions'].c.sort_order))
        return [row[0] for row in self.conn.execute(qry).fetchall()]

    def resolve_distribution_alias(self, distribution):
        qry = select(
            [self.tables['distribution_aliases'].c.distribution],
            self.tables['distribution_aliases'].c.alias == distribution)
        result = self.conn.execute(qry).fetchall()
        if len(result) == 0:
            return distribution
        else:
            return result[0][0]

    def dist_arch_exists(self, dist, arch):
        if self._dist_archs is None:
            qry = select(
                [self.tables['distribution_architectures_statistics']])
            dict = {}
            for result in self.conn.execute(qry).fetchall():
                dict[result[0:2]] = (int(result[2]), int(result[3]))
            self._dist_archs = dict

        return self._dist_archs.get((dist, arch))
